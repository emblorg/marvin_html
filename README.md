# Marvin HTML

A simple Drupal 8 theme to strip out all but the content HTML, so we can use it for
html `link` embeds.

This way you get a nice HTML stub without the usual Drupal+Views cruft:

```
<div class="exported-content">
  <aside class="embl-b-factoid">
    <h3 class="embl-b-factoid__heading">Cancer factoid: leading cause of death</h3>
    <p class="embl-b-factoid__content">Invasive cancer is the leading cause of death in the developed world and the second leading in the developing world.</p>
  </aside>
</div>
```

And that works great with the EMBL Visual Framework 2.0's patterns.

## Recommendations

- Combine with switch_page_theme module so your View pages use this theme.
- Also use views_rows_wrapper so you can more fully style the view output elements.

## Install with Composer

In your `composer.json` add:

```
"repositories": [
    {
        "type": "vcs",
        "no-api": true,
        "url": "https://gitlabci.ebi.ac.uk/emblorg/marvin_html.git"
    }
],
```

Then:

`composer require emblorg/marvin_html`
